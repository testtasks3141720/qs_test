<?php

namespace App\Services;
use App\Models\Plan;

class PlanService
{
    public function addPlan($name, $price, $available_publications)
    {
        $plan = new Plan;
        $plan->name = $name;
        $plan->price = $price;
        $plan->available_publications = $available_publications;
        $plan->save();

        return $plan;
    }


}