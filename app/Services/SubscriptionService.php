<?php

namespace App\Services;
use App\Gateway\StripeGateway;
use App\Models\Subscription;
use App\Models\Plan;

class SubscriptionService
{
    public function __construct(
        protected StripeGateway $stripeGateway,
    ) {}

    public function addSubscription($user_id, $plan_id)
    {
        $subscription = new Subscription;
        $subscription->user_id = $user_id;
        $subscription->plan_id = $plan_id;
        $subscription->save();
        
        return $subscription;
    }

    public function paySubscription($subscription, $pay_tocken)
    {
        $plan = Plan::findOrFail($subscription->plan_id);
        $payment_id = $this->stripeGateway->subscriptionPaymentCreate($plan->name, $plan->price, $pay_tocken);

        $subscription->stripe_payment_id = $payment_id;
        $subscription->save();

        return $this->tryToActivateSubscription($subscription);
    }

    public function tryToActivateSubscription($subscription)
    {
        if ($this->stripeGateway->subscriptionPaymentCheck($subscription->stripe_payment_id)) {
            $subscription->active = true;
            $subscription->expires_at = \Carbon\Carbon::now();
            $subscription->save();
        }

        return $subscription;
    }

}