<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind('subscriptions-service', function ($app) {
            return new \App\Services\SubscriptionService(new \App\Gateway\StripeGateway);
        });

        $this->app->bind('plans-service', function ($app) {
            return new \App\Services\PlanService();
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
