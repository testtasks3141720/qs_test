<?php

namespace App\Gateway;

class StripeGateway
{   
    private $stripe;

    public function __construct()
    {
        $this->stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
    }

    public function subscriptionPaymentCreate($plan_name, $plan_price, $pay_tocken) : string
    {
        $payment = $this->stripe->charges->create([
            "amount" => $plan_price,
            "currency" => "usd",
            "source" => $pay_tocken,
            "description" => "Payment: ".$plan_name
          ]);

        return $payment->id;
    }

    public function subscriptionPaymentCheck($payment_id)
    {
        $payment = $this->stripe->charges->retrieve(
            $payment_id,
            []
          );

        return $payment->paid;
    }
}