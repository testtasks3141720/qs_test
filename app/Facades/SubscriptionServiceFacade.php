<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class SubscriptionServiceFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'subscriptions-service';
    }
}