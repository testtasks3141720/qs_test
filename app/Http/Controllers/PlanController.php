<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Facades\PlanServiceFacade;

class PlanController extends Controller
{
    /**
     * @queryParam name
     * @queryParam price
     * @queryParam available_publications
     */
    public function store(Request $request)
    {
        $plan = PlanServiceFacade::addPlan($request->name, $request->price, $request->available_publications);

        return $plan->toJson();
    }
}
