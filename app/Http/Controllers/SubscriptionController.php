<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Plan;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Facades\SubscriptionServiceFacade;

class SubscriptionController extends Controller
{
    /**
     * @queryParam plan_id
    */
    public function store(Request $request, $user_id)
    {
        $user = User::findOrFail($user_id);
        $plan = Plan::findOrFail($request->plan_id);
        $subscription = SubscriptionServiceFacade::addSubscription($user->id, $plan->id);

        return $subscription->toJson();
    }

    /**
     * @queryParam pay_tocken
    */
    public function pay(Request $request, $user_id)
    {
        $subscription = (User::findOrFail($user_id))->subscription;
        $subscription = SubscriptionServiceFacade::paySubscription($subscription, $request->pay_tocken);

        return new JsonResponse(['stripe_payment_id' => $subscription->stripe_payment_id]);
    }

    /**
     * Check activation
    */
    public function active($user_id)
    {
        $subscription = (User::findOrFail($user_id))->subscription;
        $subscription = $subscription->active ? $subscription : SubscriptionServiceFacade::tryToActivateSubscription($subscription);

        return new JsonResponse(['active' => $subscription->active]);
    }
}
