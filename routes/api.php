<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PlanController;
use App\Http\Controllers\SubscriptionController;
use App\Http\Controllers\UserController;

Route::resource('plans', PlanController::class)->only(['store']);

Route::resource('users', UserController::class)->only(['store']);

Route::post('users/{user_id}/subscription', [SubscriptionController::class, 'store']);
Route::post('users/{user_id}/subscription/payment', [SubscriptionController::class, 'pay']);
Route::get('users/{user_id}/subscription/active', [SubscriptionController::class, 'active']);

// TODO
// Route::apiResources(['users.publication' => PublicationController::class]);
