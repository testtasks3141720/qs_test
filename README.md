## Realized:
- Create subscription plans
- Create subscriptions
- Payment for subscriptions

## All logic in:
- routes\api.php
- app\Http\Controllers\PlanController.php
- app\Http\Controllers\SubscriptionController.php
- app\Services\PlanService.php
- app\Services\SubscriptionService.php
- app\Gateway\StripeGateway.php

Stripe payment system is used

## Examples:

Create plan
POST /api/plans?name=starting&price=2000&available_publications=20

Create user
POST /api/users?name=uname1&email=u0mail@mail.loc&password=12345678

Subscribe
POST /api/users/1/subscription?plan_id=1

Pay subscribe
POST /api/users/1/subscription/payment?pay_tocken=tok_amex

Check status subscribe(wich check payd)
GET /api/users/1/subscription/active